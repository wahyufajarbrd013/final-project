import {
    mount
} from '@vue/test-utils'
import App from './../../../resources/js/src/App.vue'
import Navbar from './../../../resources/js/src/components/Navbar.vue'
import Sidebar from './../../../resources/js/src/components/Sidebar.vue'
import Footer from './../../../resources/js/src/components/Footer.vue'

describe('Mounted App', () => {
    const wrapper = mount(App,Navbar,Sidebar,Footer);

    test('does a wrapper exist', () => {
        expect(wrapper.exists()).toBe(true)
    })
})