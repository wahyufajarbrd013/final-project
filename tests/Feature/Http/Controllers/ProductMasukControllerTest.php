<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Product_masuk;
use App\Models\Supplier;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductMasukControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/product_masuks');
        $response->assertStatus(200);
    }

    public function testGetData()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link = "/api/categorys/".strval($create_cat->id);
        
        $sup = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_sup = Supplier::factory()->create($sup);
        $link2 = "/api/suppliers/".strval($create_sup->id);

        $pro = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_pro = Product::factory()->create($pro);
        $link3 = "/api/products/".strval($create_pro->id);
        
        $reg = [
            'product_id' => $create_pro->id,
            'supplier_id' => $create_sup->id,
            'qty' => 10,
            'harga_modal' => 1000,
            'tanggal' => '2022-12-01'
        ];

        $create_reg = Product_masuk::factory()->create($reg);
        $link4 = "/api/product_masuks/".strval($create_reg->id);
        $response = $this->get($link4);
        $response->assertStatus(200);
        $this->delete($link);
        $this->delete($link2);
        $this->delete($link3);
        $this->delete($link4);
    }

    public function testCreate()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link = "/api/categorys/".strval($create_cat->id);
        
        $sup = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_sup = Supplier::factory()->create($sup);
        $link2 = "/api/suppliers/".strval($create_sup->id);

        $pro = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_pro = Product::factory()->create($pro);
        $link3 = "/api/products/".strval($create_pro->id);
        
        $reg = [
            'product_id' => $create_pro->id,
            'supplier_id' => $create_sup->id,
            'qty' => 10,
            'harga_modal' => 1000,
            'tanggal' => '2022-12-01'
        ];

        $response = $this->post('/api/products', $reg);
        $response->assertStatus(302);
        $this->delete($link);
        $this->delete($link2);
        $this->delete($link3);
    }

    public function testUpdate()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link = "/api/categorys/".strval($create_cat->id);
        
        $sup = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_sup = Supplier::factory()->create($sup);
        $link2 = "/api/suppliers/".strval($create_sup->id);

        $pro = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_pro = Product::factory()->create($pro);
        $link3 = "/api/products/".strval($create_pro->id);
        
        $reg = [
            'product_id' => $create_pro->id,
            'supplier_id' => $create_sup->id,
            'qty' => 10,
            'harga_modal' => 1000,
            'tanggal' => '2022-12-01'
        ];
        $create_reg = Product_masuk::factory()->create($reg);
        $link4 = "/api/product_masuks/".strval($create_reg->id);
        $newreg = [
            'product_id' => $create_pro->id,
            'supplier_id' => $create_sup->id,
            'qty' => 20,
            'harga_modal' => 1000,
            'tanggal' => '2022-12-01'
        ];
        $response = $this->patch($link4, $newreg);
        $response->assertStatus(200);
        $this->delete($link);
        $this->delete($link2);
        $this->delete($link3);
        $this->delete($link4);
    }

    public function testDestroy()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link = "/api/categorys/".strval($create_cat->id);
        
        $sup = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_sup = Supplier::factory()->create($sup);
        $link2 = "/api/suppliers/".strval($create_sup->id);

        $pro = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_pro = Product::factory()->create($pro);
        $link3 = "/api/products/".strval($create_pro->id);
        
        $reg = [
            'product_id' => $create_pro->id,
            'supplier_id' => $create_sup->id,
            'qty' => 10,
            'harga_modal' => 1000,
            'tanggal' => '2022-12-01'
        ];

        $create_reg = Product_masuk::factory()->create($reg);
        $link4 = "/api/product_masuks/".strval($create_reg->id);
        $response = $this->delete($link4);
        $response->assertStatus(200);
        $this->delete($link);
        $this->delete($link2);
        $this->delete($link3);
    }

    public function testGenerateLaporanMasuk(){
        $response = $this->get('/api/report-masuk');
        $response->assertStatus(200);
    }
}
