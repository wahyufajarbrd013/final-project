<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/products');
        $response->assertStatus(200);
    }

    public function testGetInfoProducts(){
        $response = $this->get('/api/getInfoProducts');
        $response->assertStatus(200);
    }

    public function testGetData()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link2 = "/api/categorys/".strval($create_cat->id);

        $reg = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_reg = Product::factory()->create($reg);
        
        $link = "/api/products/".strval($create_reg->id);
        $response = $this->get($link);
        $response->assertStatus(200);
        $this->delete($link);
        $this->delete($link2);
    }

    public function testCreate()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link2 = "/api/categorys/".strval($create_cat->id);

        $reg = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $response = $this->post('/api/products', $reg);
        $response->assertStatus(200);
        $this->delete($link2);
    }

    public function testUpdate()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link2 = "/api/categorys/".strval($create_cat->id);

        $reg = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_reg = Product::factory()->create($reg);
        $link = "/api/products/".strval($create_reg->id);
        $newreg = [
            'product_nama'  => 'test2',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $response = $this->patch($link, $newreg);
        $response->assertStatus(200);
        $this->delete($link);
        $this->delete($link2);
    }

    public function testDestroy()
    {
        $cat = [
            'category_nama' => 'test',
        ];

        $create_cat = Category::factory()->create($cat);
        $link2 = "/api/categorys/".strval($create_cat->id);

        $reg = [
            'product_nama'  => 'test',
            'product_harga' => 1000,
            'product_qty'   => 10,
            'category_id'   => $create_cat->id
        ];
        $create_reg = Product::factory()->create($reg);
        $link = "/api/products/".strval($create_reg->id);
        $response = $this->delete($link);
        $response->assertStatus(200);
        $this->delete($link2);
    }
}
