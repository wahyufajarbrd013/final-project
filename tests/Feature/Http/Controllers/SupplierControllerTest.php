<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SupplierControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/suppliers');
        $response->assertStatus(200);
    }
    public function testGetData()
    {
        $reg = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_reg = Supplier::factory()->create($reg);
        $link = "/api/suppliers/".strval($create_reg->id);
        $response = $this->get($link);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testCreate()
    {
        $reg = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];
        $response = $this->post('/api/suppliers', $reg);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $reg = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_reg = Supplier::factory()->create($reg);
        $link = "/api/suppliers/".strval($create_reg->id);
        $newreg = [
            'supplier_nama' => 'test2',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];
        $response = $this->patch($link, $newreg);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testDestroy()
    {
        $reg = [
            'supplier_nama' => 'test',
            'supplier_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'supplier_telepon' => '232323444343'
        ];

        $create_reg = Supplier::factory()->create($reg);
        $link = "/api/suppliers/".strval($create_reg->id);
        $response = $this->delete($link);
        $response->assertStatus(200);
    }
}
