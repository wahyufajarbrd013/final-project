<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/categorys');
        $response->assertStatus(200);
    }

    public function testGetData()
    {
        $reg = [
            'category_nama' => 'test',
        ];

        $create_reg = Category::factory()->create($reg);
        $link = "/api/categorys/".strval($create_reg->id);
        $response = $this->get($link);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testCreate()
    {
        $reg = [
            'category_nama' => 'test',
        ];
        $response = $this->post('/api/categorys', $reg);
        $response->assertStatus(201);
    }

    public function testUpdate()
    {
        $reg = [
            'category_nama' => 'test',
        ];

        $create_reg = Category::factory()->create($reg);
        $link = "/api/categorys/".strval($create_reg->id);
        $newreg = [
            'category_nama' => 'test2',
        ];
        $response = $this->patch($link, $newreg);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testDestroy()
    {
        $reg = [
            'category_nama' => 'test',
        ];

        $create_reg = Category::factory()->create($reg);
        $link = "/api/categorys/".strval($create_reg->id);
        $response = $this->delete($link);
        $response->assertStatus(200);
    }

}
