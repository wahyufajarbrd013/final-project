<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/customers');
        $response->assertStatus(200);
    }

    public function testGetData()
    {
        $reg = [
            'customer_nama' => 'test',
            'customer_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'customer_telepon' => '232323444343'
        ];

        $create_reg = Customer::factory()->create($reg);
        $link = "/api/customers/".strval($create_reg->id);
        $response = $this->get($link);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testCreate()
    {
        $reg = [
            'customer_nama' => 'test',
            'customer_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'customer_telepon' => '232323444343'
        ];
        $response = $this->post('/api/customers', $reg);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $reg = [
            'customer_nama' => 'test',
            'customer_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'customer_telepon' => '232323444343'
        ];

        $create_reg = Customer::factory()->create($reg);
        $link = "/api/customers/".strval($create_reg->id);
        $newreg = [
            'customer_nama' => 'test2',
            'customer_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'customer_telepon' => '232323444343'
        ];
        $response = $this->patch($link, $newreg);
        $response->assertStatus(200);
        $this->delete($link);
    }

    public function testDestroy()
    {
        $reg = [
            'customer_nama' => 'test',
            'customer_alamat' => 'test',
            'email' => 'abc@gmail.com',
            'customer_telepon' => '232323444343'
        ];

        $create_reg = Customer::factory()->create($reg);
        $link = "/api/customers/".strval($create_reg->id);
        $response = $this->delete($link);
        $response->assertStatus(200);
    }
}
