<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_masuks', function (Blueprint $table) {
            $table->integer('harga_modal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('product_masuks', 'harga_modal')) {
            Schema::table('product_masuks', function (Blueprint $table) {
                $table->dropColumn('harga_modal');
            });
        }
    }
};
