# Final Project Best Group 1 - Iventory
<img src="public/assets/img/readme/dashboard.png" height="200">

## Deskripsi
Iventory adalah sebuah aplikasi berbasis website yang berguna dalam mengatur penyimpanan barang masuk dan barang keluar dari sebuah warung.

## Fitur
Iventory menghadirkan 6 fitur utama yaitu:
- Product
    - Menampilkan list product yang dipunya
    - Menambahkan data product baru
    - Mengedit data product lama
    - Menghapus data product
- Category
    - Menampilkan list category yang dipunya
    - Menambahkan data category baru
    - Mengedit data category lama
    - Menghapus data category
- Customer
    - Menampilkan list customer yang dipunya
    - Menambahkan data customer baru
    - Mengedit data customer lama
    - Menghapus data customer
- Supplier
    - Menampilkan list supplier yang dipunya
    - Menambahkan data supplier baru
    - Mengedit data supplier lama
    - Menghapus data supplier
- Product Masuk
    - Menampilkan list product masuk yang dipunya
    - Menambahkan data product masuk baru
    - Mengedit data product masuk lama
    - Menghapus data product masuk
    - Membuat laporan product masuk
- Product Keluar 
    - Menampilkan list product keluar yang dipunya
    - Menambahkan data product keluar baru
    - Mengedit data product keluar lama
    - Menghapus data product keluar
    - Membuat laporan product keluar

## Cara Install
1. Clone the github repo:

    ```bash
    git clone https://gitlab.com/wahyufajarbrd013/final-project.git
    ```
2. Setelah clone, masuk ke directorynya bisa dengan ketik perintah dibawah pada bash kamu:

    ```bash
    cd final-project
    ```
3. Install the project dependencies:
    ```bash
    composer install
    ```
4. Jangan lupa copy .env.example ke root folder dan ganti nama menjadi .env setelah itu lakukan config seperti ini:
   ```bash
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=final_project
    DB_USERNAME=root
    DB_PASSWORD=
   ```
5. Running XAMPP or MySQL kamu dan buat sebuah Database kosong dengan nama final_project

6. Buat tabel pada database yang telah dibuat dengan menjalankan perintah berikut pada bash kamu:
    ```bash
    php artisan migrate
    ```
7. Isi database yang telah dibuat dengan menjalankan perintah berikut pada bash kamu:
    ```bash
    php artisan db:seed
    ```
8. Setup ile package.json kamu seperti berikut:
    ```bash
    {
        "private": true,
        "scripts": {
            "test:unit": "vue-cli-service test:unit",
            "dev": "npm run development",
            "development": "mix",
            "hot": "mix watch --hot",
            "prod": "npm run production",
            "production": "mix --production",
            "watch": "mix watch",
            "watch-poll": "mix watch -- --watch-options-poll=1000",
            "test": "jest"
        },
        "devDependencies": {
            "@babel/core": "^7.19.6",
            "@babel/preset-env": "^7.19.4",
            "@vue/cli-plugin-unit-jest": "^5.0.8",
            "@vue/server-test-utils": "^1.3.0",
            "@vue/test-utils": "^1.3.0",
            "axios": "^0.25.0",
            "babel-core": "^7.0.0-bridge.0",
            "babel-jest": "^29.2.2",
            "browser-sync": "^2.27.10",
            "browser-sync-webpack-plugin": "^2.3.0",
            "eslint": "^8.12.0",
            "eslint-plugin-vue": "^9.7.0",
            "jest": "^27.5.1",
            "jsdom-global": "^3.0.2",
            "laravel-mix": "^6.0.6",
            "lodash": "^4.17.19",
            "postcss": "^8.4.19",
            "sass": "^1.49.9",
            "sass-loader": "^12.6.0",
            "vue": "^2.7.14",
            "vue-jest": "^3.0.7",
            "vue-loader": "^15.10.1",
            "vue-router": "^3.5.3",
            "vue-template-compiler": "^2.7.14"
        },
        "dependencies": {
            "@vue/cli-service": "^5.0.8",
            "vue-axios": "^2.1.5",
            "vuex": "3.1.1"
        },
        "jest": {
            "moduleFileExtensions": [
                "js",
                "json",
                "vue"
            ],
            "transform": {
                ".*\\.(vue)$": "vue-jest",
                ".*\\.(js)$": "babel-jest"
            },
            "testEnvironment": "jsdom"
        },
        "babel": {
            "presets": [
                "@babel/preset-env"
            ]
        }
    }

    ```
9. Install npm dependencies
    ```bash
    npm install
    ```
10. Jalankan laravel server:
    ```bash
    php artisan serve
    ```
11. Run watch:
    ```bash
    npm run watch
    ```
12. Terakhir, buka browser kamu dan ketik localhost:8000, nikmatin aplikasinya!

## Unit Test

ini adalah hasil coverage unit test untuk controller menggunakan phpunit

<img src="public/assets/img/readme/unit-test.png" height="200">

untuk melakukan unit test dengan phpunit bisa menjalankan perintah berikut pada bash kamu:

```bash
composer test:coverage
```

untuk melakukan unit test dengan jest bisa menjalankan perintah berikut pada bash kamu:

```bash
npm run test
```

## License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


# Framework Contribution

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel is accessible, powerful, and provides tools required for large, robust applications.

<p align="center" style="margin-top:50px;"><a href="https://laravel.com" target="_blank"><img src="https://camo.githubusercontent.com/c8f91d18976e27123643a926a2588b8d931a0292fd0b6532c3155379e8591629/68747470733a2f2f7675656a732e6f72672f696d616765732f6c6f676f2e706e67" width="100" alt="Laravel Logo"></a></p>

## About Vue

Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces. It is designed from the ground up to be incrementally adoptable, and can easily scale between a library and a framework depending on different use cases. It consists of an approachable core library that focuses on the view layer only, and an ecosystem of supporting libraries that helps you tackle complexity in large Single-Page Applications.