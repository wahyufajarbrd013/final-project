<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductKeluarController;
use App\Http\Controllers\ProductMasukController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\SupplierController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->group(function () {

    //category
    Route::resource('categorys', CategoryController::class);

    //product
    Route::resource('products', ProductController::class);

    //customers
    Route::resource('customers', CustomerController::class);

    //product keluar
    Route::resource('product_keluars', ProductKeluarController::class);

    //supplier
    Route::resource('suppliers', SupplierController::class);

    //product masuk
    Route::resource('product_masuks', ProductMasukController::class);

    Route::get('getInfoProducts',[ProductController::class, 'getInfoProducts']);

    Route::get('report-masuk', [PDFController::class, 'generateLaporanMasuk']);

    Route::get('report-keluar', [PDFController::class, 'generateLaporanKeluar']);

});