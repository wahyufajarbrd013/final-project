import Vue from 'vue';

const mutations = {
    SET_CATEGORIES(state, value) {
        state.categories = value
    },
    SET_PRODUCTS(state, value) {
        state.products = value
    },

    SET_CUSTOMERS(state, value) {
        state.customers = value
    },

    SET_SUPPLIERS(state, value) {
        state.suppliers = value
    },

    SET_PRODUCT_MASUKS(state, value) {
        state.product_masuks = value
    },

    SET_PRODUCT_KELUARS(state, value) {
        state.product_keluars = value
    },

    SET_CATEGORY_DELETED(state, value) {
    const index = state.categories.findIndex(category => category.id === value);
    state.categories.splice(index, 1);
    },

    SET_PRODUCT_DELETED(state, value) {
    const index = state.products.findIndex(product => product.id === value);
    state.products.splice(index, 1);
    },

    SET_CUSTOMER_DELETED(state, value) {
    const index = state.customers.findIndex(customer => customer.id === value);
    state.customers.splice(index, 1);
    },

    SET_SUPPLIER_DELETED(state, value) {
    const index = state.suppliers.findIndex(supplier => supplier.id === value);
    state.suppliers.splice(index, 1);
    },

    SET_PRODUCT_MASUK_DELETED(state, value) {
    const index = state.product_masuks.findIndex(product_masuk => product_masuk.id === value);
    state.product_masuks.splice(index, 1);
    },

    SET_PRODUCT_KELUAR_DELETED(state, value) {
    const index = state.product_keluars.findIndex(product_keluar => product_keluar.id === value);
    state.product_keluars.splice(index, 1);
    },

    SET_INFORMATION_PRODUCTS(state, value){
        state.information_products = value
    }
}
export default mutations
