<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_keluar extends Model
{
    use HasFactory;

    protected $table = 'product_keluars';
    protected $guarded = ['id'];
    protected $fillable = [
        'product_id',
        'customer_id',
        'qty',
        'harga_jual',
        'tanggal'
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
