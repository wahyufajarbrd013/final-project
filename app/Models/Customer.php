<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';
    protected $guarded = ['id'];
    protected $fillable = [
        'customer_nama',
        'customer_alamat',
        'email',
        'customer_telepon'
    ];

    public function product_keluar()
    {
        return $this->hasmany(Product_keluar::class);
    }
}