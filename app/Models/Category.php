<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $guarded = ['id'];
    protected $fillable = [
        'category_nama'
    ];

    public function product()
    {
        return $this->hasmany(Product::class);
    }
}