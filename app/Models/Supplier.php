<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $table = 'suppliers';
    protected $guarded = ['id'];
    protected $fillable = [
        'supplier_nama',
        'supplier_alamat',
        'email',
        'supplier_telepon'
    ];

    public function product_masuk()
    {
        return $this->hasmany(Product_masuk::class);
    }
}