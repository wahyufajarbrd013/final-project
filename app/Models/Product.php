<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $guarded = ['id'];
    protected $fillable = [
        'product_nama',
        'category_id',
        'product_harga',
        'product_qty'
    ];

    public function product_masuk()
    {
        return $this->hasmany(Product_masuk::class);
    }

    public function product_keluar()
    {
        return $this->hasmany(Product_keluar::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}