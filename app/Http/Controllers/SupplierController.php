<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    public function index()
    {
        $supplier = Supplier::orderBy('supplier_nama')->get();

        return response()->json(['suppliers' => $supplier], 200);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'supplier_nama' => 'required|max:255',
            'supplier_alamat' => 'required',
            'email' => 'required',
            'supplier_telepon' => 'required|max:20'
        ]);
        Supplier::create($validatedData);
        return response()->json('Supplier Ditambah!');
    }

    public function show(Supplier $supplier)
    {
        return response()->json($supplier);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'supplier_nama' => 'required|max:255',
            'supplier_alamat' => 'required',
            'email' => 'required',
            'supplier_telepon' => 'required|max:20'
        ]);
        $supplier = Supplier::findOrFail($id);
        $supplier->update($validatedData);
        return response()->json('Supplier Diedit!');
    }

    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();
        return response()->json('Supplier Dihapus!');
    }
}