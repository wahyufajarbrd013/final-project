<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PDFController extends Controller
{

    public function generateLaporanMasuk(){
        $product_masuks = DB::table('product_masuks')
            ->join('products', 'product_masuks.product_id', 'products.id')
            ->join('suppliers', 'product_masuks.supplier_id', 'suppliers.id')
            ->select('product_masuks.*', 'products.product_nama', 'suppliers.supplier_nama')
            ->orderBy('product_masuks.tanggal', 'desc')
            ->get();
        
        $total_jenis_produk = DB::table('product_masuks')
            ->distinct()
            ->count('product_id');
        
        $total_barang_masuk = DB::table('product_masuks')
            ->sum('product_masuks.qty');

        $total_pengeluaran = 0;

        foreach ($product_masuks as $product) {
            $total = $product->qty * $product->harga_modal;
            $total_pengeluaran += $total;
        }

        $total_pengeluaran = number_format($total_pengeluaran,0, ',' , '.');

        $data = [
            'no' => 0,
            'products' => $product_masuks,
            'date' => date('m/d/Y'),
            'jenis_produk' => $total_jenis_produk,
            'barang_masuk' => $total_barang_masuk,
            'pengeluaran' => $total_pengeluaran
        ];
          
        $pdf = PDF::loadView('laporan-masuk', $data);
        $namaFile = 'laporan-masuk-produk-' . date('m/d/Y');
        return $pdf->download($namaFile.'.pdf');
    }

    public function generateLaporanKeluar(){
        $product_keluars = DB::table('product_keluars')
            ->join('products', 'product_keluars.product_id', 'products.id')
            ->join('customers', 'product_keluars.customer_id', 'customers.id')
            ->select('product_keluars.*', 'products.product_nama', 'customers.customer_nama')
            ->orderBy('product_keluars.tanggal', 'desc')
            ->get();
            
        $total_jenis_produk = DB::table('product_keluars')
            ->distinct()
            ->count('product_id');
        
        $total_barang_keluar = DB::table('product_keluars')
            ->sum('product_keluars.qty');

        $total_pemasukan = 0;

        foreach ($product_keluars as $product) {
            $total = $product->qty * $product->harga_jual;
            $total_pemasukan += $total;
        }

        $total_pemasukan = number_format($total_pemasukan,0, ',' , '.');

        $data = [
            'no' => 0,
            'products' => $product_keluars,
            'date' => date('m/d/Y'),
            'jenis_produk' => $total_jenis_produk,
            'barang_keluar' => $total_barang_keluar,
            'pemasukan' => $total_pemasukan
        ];
          
        $pdf = PDF::loadView('laporan-keluar', $data);
        
        $namaFile = 'laporan-keluar-produk-' . date('m/d/Y');

        return $pdf->download($namaFile.'.pdf');
    }

}
