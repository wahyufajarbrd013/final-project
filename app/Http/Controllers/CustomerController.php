<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function index()
    {
        $customer = Customer::orderBy('customer_nama')->get();

        return response()->json(['customers' => $customer], 200);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'customer_nama' => 'required|max:255',
            'customer_alamat' => 'required',
            'email' => 'required',
            'customer_telepon' => 'required|max:20'
        ]);
        Customer::create($validatedData);
        return response()->json('Customer Ditambah!');
    }

    public function show(Customer $customer)
    {
        return response()->json($customer);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'customer_nama' => 'required|max:255',
            'customer_alamat' => 'required',
            'email' => 'required',
            'customer_telepon' => 'required|max:20'
        ]);
        $customer = Customer::findOrFail($id);
        $customer->update($validatedData);
        return response()->json('Customer Diedit!');
    }

    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
        return response()->json('Customer Dihapus!');
    }
}