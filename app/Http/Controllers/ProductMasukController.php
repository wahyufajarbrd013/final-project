<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Product_masuk;
use Illuminate\Support\Facades\DB;

class ProductMasukController extends Controller
{
    private function updateQty($id, $qty){
        $product = Product::findOrFail($id);
        $product->product_qty = $product->product_qty + $qty;
        $product->update();
    }

    public function index()
    {
        $product_masuks = DB::table('product_masuks')
            ->join('products', 'product_masuks.product_id', 'products.id')
            ->join('suppliers', 'product_masuks.supplier_id', 'suppliers.id')
            ->select('product_masuks.*', 'products.product_nama', 'suppliers.supplier_nama')
            ->orderBy('product_masuks.tanggal', 'desc')
            ->get();

        return response()->json($product_masuks);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'product_id' => 'required',
            'supplier_id' => 'required',
            'qty' => 'required',
            'harga_modal' => 'required',
            'tanggal' => 'required'
        ]);
        $this->updateQty($validatedData['product_id'], $validatedData['qty']);
        $product_masuk = Product_masuk::create($validatedData);
        return response()->json([
            'message' => 'Product masuk Created Successfully!!',
            'product_masuks' => $product_masuk
        ]);
    }

    public function show(Product_masuk $product_masuk)
    {
        return response()->json($product_masuk);
    }

    public function update(Request $request, $id)
    {   
        $validatedData = $request->validate([
            'product_id' => 'required',
            'supplier_id' => 'required',
            'qty' => 'required',
            'harga_modal' => 'required',
            'tanggal' => 'required'
        ]);

        $product_id = $validatedData['product_id'];
        $qty = (int) $validatedData['qty'];

        $product_masuk = Product_masuk::findOrFail($id);
        if($product_masuk->qty != $qty){
            $qty = $qty - $product_masuk->qty;
            $this->updateQty($product_id, $qty);
        }
        $product_masuk->update($validatedData);
        return response()->json('Data Produk Masuk Diedit!');
    }

    public function destroy($id)
    {
        $product_masuk = Product_masuk::findOrFail($id);
        $product_id = $product_masuk->product_id;
        $product = Product::findOrFail($product_id);
        if($product->product_qty >= $product_masuk->qty){
            $product->product_qty -= $product_masuk->qty;
        }else{
            $product->product_qty = 0;
        }
        $product->save();
        $product_masuk->delete();
        return response()->json('Data Produk Masuk Dihapus!');
    }
}