<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Product_keluar;
use Illuminate\Support\Facades\DB;

class ProductKeluarController extends Controller
{
    private function updateQty($id, $qty){
        $product = Product::findOrFail($id);
        $product->product_qty = $product->product_qty - $qty;
        $product->update();
    }
    
    public function index()
    {
        $product_keluars = DB::table('product_keluars')
            ->join('products', 'product_keluars.product_id', 'products.id')
            ->join('customers', 'product_keluars.customer_id', 'customers.id')
            ->select('product_keluars.*', 'products.product_nama', 'customers.customer_nama')
            ->orderBy('product_keluars.tanggal', 'desc')
            ->get();

        return response()->json($product_keluars);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'product_id' => 'required',
            'customer_id' => 'required',
            'qty' => 'required',
            'tanggal' => 'required'
        ]);

        $product_id = $validatedData['product_id'];
        $qty = (int) $validatedData['qty'];
        $product = Product::findOrFail($product_id);
        $validatedData['harga_jual'] = $product->product_harga;
        $product_keluar = Product_keluar::create($validatedData);
        if($product->product_qty >= $qty){
            $this->updateQty($product_id, $qty);
        }else{
            $this->updateQty($product_id, $product->product_qty);
        }

        return response()->json([
            'message' => 'Product keluar Created Successfully!!',
            'product_keluars' => $product_keluar,
        ]);
    }

    public function show(Product_keluar $product_keluar)
    {
        return response()->json($product_keluar);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'product_id' => 'required',
            'customer_id' => 'required',
            'qty' => 'required',
            'harga_jual' => 'required',
            'tanggal' => 'required'
        ]);
        $product_id = $validatedData['product_id'];
        $qty = (int) $validatedData['qty'];

        $product_keluar = Product_keluar::findOrFail($id);
        
        $product = Product::findOrFail($product_id);
        if($product->product_qty >= $qty){
            $qty = $qty - $product_keluar->qty;
            $this->updateQty($product_id, $qty);
        }else{
            $this->updateQty($product_id, $product->product_qty);
        }

        $product_keluar->update($validatedData);
        return response()->json('Data Produk keluar Diedit!');
    }

    public function destroy($id)
    {
        $product_keluar = Product_keluar::findOrFail($id);
        $product_id = $product_keluar->product_id;
        $product = Product::findOrFail($product_id);
        $product->product_qty += $product_keluar->qty;
        $product->save();
        $product_keluar->delete();
        return response()->json('Data Produk keluar Dihapus!');
    }
}